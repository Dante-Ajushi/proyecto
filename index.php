<!DOCTYPE html>
<html>
<head>
	<title>Color Picker</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<canvas class="canvas" id="mesh"></canvas>
	<div>
		<fieldset>
			<legend>RGB</legend>
			<label for="r">R</label>
			<input type="range" min="0" max="255" step="1" name="r" class="rgb" id="r" value="80"/> <output id="red" for="r"></output>
			<label for="g">G</label>
			<input type="range" min="0" max="255" step="1" name="g" class="rgb" id="g" value="80"/> <output id="green" for="g"></output>
			<label for="b">B</label>
			<input type="range" min="0" max="255" step="1" name="b" class="rgb" id="b" value="80"/> <output id="blue" for="b"></output>
		</fieldset>

		<fieldset>
			<legend>Hexadecimal</legend>
			<input type="text" name="hex" class="hex" id="h"/>
			<label for="hex">Result: #</label><output class="hex" id="hex" for="hex"></output>
		</fieldset>
	</div>
	
	<script type="text/javascript" src="mesh.js"></script>
</body>
</html>