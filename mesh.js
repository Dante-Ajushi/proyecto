function textureColor(r, g, b) {
	context.fillStyle = "rgb("+r+","+g+","+b+")";
	context.fill();
};
function data (r ,g , b) {
	r.innerHTML = r;
	g.innerHTML = g;
	b.innerHTML = b;
	
	red.innerHTML = r;
	green.innerHTML = g;
	blue.innerHTML = b;
};
function Torgb() {
    hex[0].value = hex[0].value.replace("#", "").trimLeft("");

	r.value = parseInt(hex[0].value.substring(0,2), 16);
    g.value = parseInt(hex[0].value.substring(2,4), 16);
    b.value = parseInt(hex[0].value.substring(4,6), 16);
    
    hex[1].value = hex[0].value.length == 6 ? hex[0].value : hex[1].value;
    textureColor(r.value, g.value , b.value);
    data(r.value, g.value, b.value);
};
function Tohexa(r,g,b) {
	r = parseInt(r,10).toString(16);
	g = parseInt(g,10).toString(16);
	b = parseInt(b,10).toString(16);
	hex[0].value = hex[1].value = ('0'+r).slice(-2) + ('0'+g).slice(-2) + ('0'+b).slice(-2);
};
function updateColor () {
    data(r.value, g.value, b.value);
	Tohexa(r.value,g.value,b.value);
    textureColor(r.value, g.value , b.value);
};

var canvas = document.getElementById('mesh');
var context = canvas.getContext('2d');
var hexin = document.getElementById('h');

var hex = document.querySelectorAll('.hex');

var r = document.getElementById('r');
var g = document.getElementById('g');
var b = document.getElementById('b');

var red = document.getElementById('red');
var green = document.getElementById('green');
var blue = document.getElementById('blue');

red.innerHTML = r.value;
green.innerHTML = g.value;
blue.innerHTML = b.value;


r.addEventListener("input", updateColor,false);
g.addEventListener("input", updateColor,false);
b.addEventListener("input", updateColor,false);
hexin.addEventListener("input", Torgb, false);

Tohexa(r.value,g.value,b.value);
textureColor(r.value, g.value , b.value);


//context.fillStyle = "rgb(200,0,0)";
//context.fillRect (75, 25, 150, 100);

//var r, g, b = 0;

context.beginPath();
context.arc(140,75,70,0,Math.PI*2,true);
context.moveTo(110,100);
context.fill();